# Trading Test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.7.

## First start
Run `npm install` to install dependencies.

## Development
Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production
Run `npm run build` to create production ready-to-use build.
