import { NgModule } from '@angular/core';
import { ExtraOptions, PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

const routerConfig: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  preloadingStrategy: PreloadAllModules,
  paramsInheritanceStrategy: 'always',
  // relativeLinkResolution: 'legacy',
};

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/trades',
  },
  {
    path: 'trades',
    loadChildren: (): Promise<unknown> => import('@modules/trades/trades.module').then((m) => m.TradesModule),
  },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, routerConfig),
  ],
  exports: [],
})
export class AppRoutingModule { }
