import {
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { TradesService } from '@modules/trades/services';
import {
  ConfirmDialogData,
  RequestError,
  RequestStateStatus,
  RequestStateStatusLabel,
  Trade,
} from '@modules/trades/types';
import { Subscriber } from 'rxjs';
import { filter, skip } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogsComponent } from '@modules/trades/components';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.styles.scss'],
})
export class TradesComponent implements OnInit, OnDestroy {
  public subscriber = new Subscriber();
  public trades: Trade[] = [];

  public constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tradesService: TradesService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) { }

  public ngOnInit(): void {
    this.subscriber.add(
      this.tradesService.list$.subscribe((trades) => {
        this.trades = trades;
      }),
    );

    this.subscriber.add(
      this.tradesService
        .status$(RequestStateStatusLabel.delete)
        .pipe(
          skip(1),
          filter((status) => status === RequestStateStatus.success),
        )
        .subscribe(() => {
          this.snackBar.open('Trade deleted successfully','ОК');
        }),
    )

    this.subscriber.add(
      this.tradesService
        .error$
        .pipe(
          skip(1),
          filter(Boolean),
        )
        .subscribe((error) => {
          this.snackBar.open((error as RequestError)?.message ?? 'Server error', 'ОК');
        }),
    )
  }

  public ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }

  public addNewTrade(): Promise<boolean> {
    return this.router.navigate(['.', 'new'],
      {
        preserveFragment: true,
        relativeTo: this.activatedRoute,
      },
    );
  }

  public editTrade(id: string): Promise<boolean> {
    return this.router.navigate(['.', id],
      {
        preserveFragment: true,
        relativeTo: this.activatedRoute,
      },
    );
  }

  public deleteTrade(id: string): void {
    this.dialog
      .open<ConfirmDialogsComponent, ConfirmDialogData, boolean>(ConfirmDialogsComponent, {
        data: {
          title: 'Delete trade',
          content: 'Do you want to delete picked trade?',
        },
      })
      .afterClosed()
      .pipe(filter(Boolean))
      .subscribe(() => this.tradesService.delete(id));
  }

  public trackByFn<R, T extends { [key: string]: R }>(index: number, item: T): R | number {
    return item?.id || index;
  }
}
