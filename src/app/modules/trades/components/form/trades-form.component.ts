import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {
  RequestError,
  RequestStateStatus,
  RequestStateStatusLabel,
  Trade,
} from '@modules/trades/types';
import { Subscriber } from 'rxjs';
import { filter, skip } from 'rxjs/operators';
import { TradesService } from '@modules/trades/services';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'trades-form',
  templateUrl: './trades-form.component.html',
  styleUrls: ['./trades-form.styles.scss'],
  providers: [
    DatePipe,
  ],
})
export class TradeFormComponent implements OnInit, OnDestroy {
  public trade: Trade | null = null;
  public id: string = this.activatedRoute.snapshot.params.id;
  public isNew: boolean = Boolean(!this.id);

  public form: FormGroup = this.formBuilder.group(
    {
      entryDate: [this.datePipe.transform(new Date(), 'yyyy-MM-dd'), Validators.required],
      entryPrice: [0, [
        Validators.min(0),
      ]],
      exitDate: [this.datePipe.transform(new Date(), 'yyyy-MM-dd'), Validators.required],
      exitPrice: [0, [
        Validators.min(0),
      ]],
    },
  );

  private subscriber = new Subscriber();

  public constructor(
    private activatedRoute: ActivatedRoute,
    private snackBar: MatSnackBar,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private tradesService: TradesService,
    private datePipe: DatePipe,
  ) {
  }

  public ngOnInit(): void {
    this.subscriber.add(
      this.tradesService.item$
        .subscribe((trade: Trade | null) => {
          this.trade = trade;

          if (!trade && !this.isNew) {
            // redirect to the list
            this.router.navigate(['..'],
              {
                preserveFragment: true,
                relativeTo: this.activatedRoute,
              },
            );
          }

          if (trade) {
            this.form.patchValue(trade);
          }
        }),
    );

    this.subscriber.add(
      this.tradesService
        .status$(this.isNew ? RequestStateStatusLabel.create : RequestStateStatusLabel.update)
        .pipe(
          skip(1),
          filter((status) => status === RequestStateStatus.success),
        )
        .subscribe(() => {
          this.snackBar.open(
            this.isNew
              ? 'Trade created successfully'
              : 'Trade updated successfully',
            'ОК',
          );
          this.goToList();
        }),
    );

    this.subscriber.add(
      this.tradesService
        .error$
        .pipe(
          skip(1),
          filter(Boolean),
        )
        .subscribe((error) => {
          this.snackBar.open((error as RequestError)?.message ?? 'Server error', 'ОК');
        }),
    )
  }

  public ngOnDestroy(): void {
    this.tradesService.itemReset();
    this.subscriber.unsubscribe();
  }

  public formControl(path: Array<string | number> | string): FormControl {
    return this.form.get(path) as FormControl;
  }

  public goToList(): Promise<boolean> {
    return this.router.navigate(['..'],
      {
        preserveFragment: true,
        relativeTo: this.activatedRoute,
      },
    );
  }

  public get headerText(): string {
    return this.isNew
      ? 'Add new trade'
      : 'Edit trade';
  }

  public get entryDate(): Date {
    return new Date(this.form.get('entryDate')?.value);
  }

  public get exitDate(): Date {
    return new Date(this.form.get('exitDate')?.value);
  }

  public get isDateRangeInvalid(): boolean {
    return this.entryDate > this.exitDate;
  }

  public submit(): void {
    if (this.isDateRangeInvalid) {
      this.snackBar.open('ERROR: Entry date should be less then Exit date', 'ОК');
      return;
    }

    if (this.form.invalid) {
      return;
    }

    const data = {
      ...this.form.value,
      entryDate: this.datePipe.transform(this.form.get('entryDate')?.value, 'yyyy-MM-dd'),
      exitDate: this.datePipe.transform(this.form.get('exitDate')?.value, 'yyyy-MM-dd'),
    };

    if (this.isNew) {
      return this.tradesService.create(data);
    }

    return this.tradesService.update(this.id, data);
  }

  public reset(): void {
    if (this.isNew && this.trade) {
      this.form.patchValue(this.trade);
      return;
    }

    this.form.patchValue({
      entryDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      entryPrice: 0,
      exitDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      exitPrice: 0,
    });
  }
}
