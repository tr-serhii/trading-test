import {
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { TradesService } from '@modules/trades/services';
import { Trade } from '@modules/trades/types';
import { Subscriber } from 'rxjs';
import {
  ApexAxisChartSeries,
  ChartType,
  ApexXAxis,
  ApexYAxis,
} from 'ng-apexcharts/lib/model/apex-types';
import { DatePipe, CurrencyPipe } from '@angular/common';

const MS_IN_DAY = 86400000; // 1000 * 60 * 60 * 24

@Component({
  selector: 'trades-chart',
  templateUrl: './trades-chart.component.html',
  providers: [
    DatePipe,
    CurrencyPipe,
  ],
})
export class TradesChartComponent implements OnInit, OnDestroy {
  public subscriber = new Subscriber();
  public trades: Trade[] = [];

  public series: ApexAxisChartSeries = [
    {
      name: 'Balance',
      data: [],
    }
  ];

  public xAxis: ApexXAxis = {
    categories: [],
    range: 10,
    labels: {
      style: {
        fontSize: '12px',
      }
    }
  };

  public yAxis: ApexYAxis = {
    labels: {
      style: {
        fontSize: '12px',
      },
      formatter: (value) => this.currencyPipe.transform(value) ?? '-',
    }
  }

  public chartOptions = {
    chart: {
      height: 300,
      type: 'line' as ChartType,
    },
    title: {
      text: 'Balance',
    },
  };

  public constructor(
    private tradesService: TradesService,
    private datePipe: DatePipe,
    private currencyPipe: CurrencyPipe,
  ) { }

  public ngOnInit(): void {
    this.subscriber.add(
      this.tradesService.list$.subscribe((trades) => {
        this.trades = trades;
        this.updateChartData();
      }),
    );
  }

  public ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }

  private updateChartData() {
    if (this.trades.length === 0) {
      return;
    }

    const startDate = this.trades
      .map((item) => item.entryDate)
      .sort((a, b) => new Date(a).valueOf() - new Date(b).valueOf())[0];

    const endDate = this.trades
      .map((item) => item.exitDate)
      .sort((a, b) => new Date(b).valueOf() - new Date(a).valueOf())[0];

    // calc diff in days between startDate and endDate
    const millisecondsDiff = new Date(endDate).valueOf() - new Date(startDate).valueOf();
    const daysDiff = millisecondsDiff / MS_IN_DAY;

    // calc days array between startDate and endDate
    const daysXAxis = [];

    for (let dayShift = 0; dayShift <= daysDiff; dayShift++) {
      const dateInMS = new Date(new Date(startDate).valueOf() + (dayShift * MS_IN_DAY));

      daysXAxis.push(this.datePipe.transform(dateInMS, 'mediumDate'));
    }

    this.xAxis = {
      ...this.xAxis,
      categories: daysXAxis,
    }

    // calc balance for each day
    this.series = [
      {
        name: 'Balance',
        data: daysXAxis.map(() => Math.round((Math.random() - Math.random()) * 100)),
      }
    ];
  }
}
