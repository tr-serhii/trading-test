import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogData } from '@modules/trades/types';

@Component({
  selector: 'confirm-dialog',
  templateUrl: './confirm-dialogs.component.html',
})
export class ConfirmDialogsComponent {
  public constructor(
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData,
  ) { }
}
