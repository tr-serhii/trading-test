import { NgModule } from '@angular/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import {
  TradeFormComponent,
  TradesComponent,
  MainComponent,
  TradesChartComponent,
  ConfirmDialogsComponent,
} from '@modules/trades/components';
import { tradeRoutes } from '@modules/trades/trades.routing';
import { tradesReducer } from '@modules/trades/store/reducers';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TradesItemEffects, TradesListEffects } from '@modules/trades/store/effects';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    TradesComponent,
    TradeFormComponent,
    MainComponent,
    TradesChartComponent,
    ConfirmDialogsComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(tradeRoutes),
    StoreModule.forFeature('trades', tradesReducer),
    EffectsModule.forFeature([TradesListEffects, TradesItemEffects]),
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDialogModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSnackBarModule,
    NgApexchartsModule,
    MatCardModule,
  ],
})
export class TradesModule {
}
