import { Injectable } from '@angular/core';
import {
  tradesCreateRequestAction,
  tradesItemRequestAction,
  tradesItemResetAction,
  tradesListRequestAction,
  tradesUpdateRequestAction,
  tradesDeleteRequestAction,
} from '@modules/trades/store/actions';
import {
  tradesStatusSelector,
  tradesStatusSomeSelector,
  selectTradesError,
  selectTradesItem,
  selectTradesList,
} from '@modules/trades/store/selectors';
import {
  TradesState,
  RequestStateStatus,
  RequestStateStatusLabel,
} from '@modules/trades/types';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TradesService {

  public list$ = this.store.select(selectTradesList);
  public item$ = this.store.select(selectTradesItem);
  public error$ = this.store.select(selectTradesError);

  public constructor(
    private store: Store<TradesState>,
  ) {
  }

  public list(reset?: boolean): void {
    this.store.dispatch(tradesListRequestAction({ payload: { reset } }));
  }

  public itemReset(): void {
    this.store.dispatch(tradesItemResetAction());
  }

  public item(id: string): void {
    this.store.dispatch(tradesItemRequestAction({ payload: id }));
  }

  public update(id: string, data: any): void {
    this.store.dispatch(tradesUpdateRequestAction({ payload: { id, data } }));
  }

  public create(data: any): void {
    this.store.dispatch(tradesCreateRequestAction({ payload: data }));
  }

  public delete(id: string): void {
    this.store.dispatch(tradesDeleteRequestAction({ payload: id }));
  }

  public status$(label: RequestStateStatusLabel): Observable<RequestStateStatus> {
    return this.store.select(tradesStatusSelector(label));
  }

  public statusSome$(status: RequestStateStatus): Observable<boolean> {
    return this.store.select(tradesStatusSomeSelector(status));
  }
}
