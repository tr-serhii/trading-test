import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { v4 as uuidV4 } from 'uuid';
import {
  Trade,
  TradeFormRequest,
  TradesListResponse,
  // TradesItemResponse,
} from '@modules/trades/types';
// import { environment } from 'environments/environment';
// import { map } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TradesApiService {
  private trades: Trade[] = [];

  public constructor(
    private httpClient: HttpClient,
  ) {
  }

  public list(): Observable<TradesListResponse> {
    //   return this.httpClient
    //     .get<TradesListResponse>(`${environment.api}/trades`)
    //     .pipe(map((response) => ({data: response.data})));

    return of({ data: this.trades });
  }

  public item(id: string): Observable<Trade> {
    //   return this.httpClient
    //     .get<TradesItemResponse>(`${environment.api}/trades/${id}`)
    //     .pipe(map((response) => response.data));

    const pickedTrade = this.trades.find((trade) => trade.id === id);

    return pickedTrade
      ? of(pickedTrade)
      : throwError(new HttpErrorResponse({ error: { message: 'Not found' }, status: 404 }));
  }

  public create(data: TradeFormRequest): Observable<Trade> {
    // return this.httpClient
    //   .post<TradesItemResponse>(`${environment.api}/trades`, data)
    //   .pipe(map((response) => response.data));

    const newTrade = {
      ...data,
      profit: data.exitPrice - data.entryPrice,
      id: uuidV4(),
    };

    this.trades.push(newTrade);
    return of(newTrade);
  }

  public update(id: string, data: TradeFormRequest): Observable<Trade> {
    // return this.httpClient
    //   .patch<TradesItemResponse>(`${environment.api}/trades/${id}`, data)
    //   .pipe(map((response) => response.data));

    const tradeToUpdate = this.trades.find((trade) => trade.id === id);

    if (!tradeToUpdate) {
      return throwError(new HttpErrorResponse({ error: { message: 'Not found' }, status: 404 }));
    }

    const updatedRawTrade = { ...tradeToUpdate, ...data };
    const updatedTrade = {
      ...updatedRawTrade,
      profit: updatedRawTrade.exitPrice - updatedRawTrade.entryPrice,
    };

    this.trades = this.trades.map((trade) => {
      if (trade.id === id) {
        return updatedTrade;
      }

      return trade;
    });

    return of(updatedTrade);
  }

  public delete(id: string): Observable<Trade> {
    // return this.httpClient
    //   .delete<TradesItemResponse>(`${environment.api}/trades/${id}`)
    //   .pipe(map((response) => response.data));

    const tradeToDelete = this.trades.find((trade) => trade.id === id);

    if (!tradeToDelete) {
      return throwError(new HttpErrorResponse({ error: { message: 'Not found' }, status: 404 }));
    }

    this.trades = this.trades.filter((trade) => trade.id !== id);

    return of (tradeToDelete);
  }
}
