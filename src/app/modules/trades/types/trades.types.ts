// Enums

export enum RequestStateStatus {
  pending = 'pending',
  success = 'success',
  error = 'error',
}

export enum RequestStateStatusLabel {
  list = 'list',
  item = 'item',
  update = 'update',
  create = 'create',
  delete = 'delete',
}

// Item

export interface Trade {
  id: string;
  entryDate: string;
  entryPrice: number;
  exitDate: string;
  exitPrice: number;
  profit: number;
}

// List

export interface TradesDictionary {
  [id: string]: Trade | undefined;
}

export interface TradesEntityData {
  entities: { trades: TradesDictionary };
  result: string | string[];
}

// Request, Response

export interface TradesListResponse {
  data: Trade[];
}

export interface TradeFormRequest {
  entryDate: string;
  entryPrice: number;
  exitDate: string;
  exitPrice: number;
}

export interface TradesItemResponse {
  data: Trade;
}

export interface RequestError {
  message: string;
}

// State

export interface RequestStatusState {
  [RequestStateStatusLabel.list]: RequestStateStatus;
  [RequestStateStatusLabel.item]: RequestStateStatus;
  [RequestStateStatusLabel.update]: RequestStateStatus;
  [RequestStateStatusLabel.create]: RequestStateStatus;
  [RequestStateStatusLabel.delete]: RequestStateStatus;
}

export interface TradesState {
  ids: string[];
  entities: TradesDictionary;
  error: RequestError | null;
  status: RequestStatusState;
  selectId: string | null;
}

// Components

export interface ConfirmDialogData {
  title: string;
  content: string;
}
