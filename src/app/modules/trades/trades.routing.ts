import { Route } from '@angular/router';
import {
  TradesComponent,
  TradeFormComponent,
  MainComponent,
} from '@modules/trades/components';
import {
  TradesListResolver,
  TradesItemResolver,
} from '@modules/trades/resolvers';

export const tradeRoutes: Route[] = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        resolve: {
          list: TradesListResolver,
        },
        component: TradesComponent,
      },
      {
        path: 'new',
        component: TradeFormComponent,
        resolve: {
          item: TradesItemResolver,
        },
      },
      {
        path: ':id',
        component: TradeFormComponent,
        resolve: {
          item: TradesItemResolver,
        },
      }
    ],
  },
];
