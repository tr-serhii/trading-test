import { tradesDeleteSuccessAction, tradesListSuccessAction } from '@modules/trades/store/actions';
import { createReducer, on } from '@ngrx/store';

export const tradesIdsReducer = createReducer<string[]>(
  [],
  on(
    tradesListSuccessAction,
    (state, action) => (
      action.payload.result as string[]
    ),
  ),

  on(
   tradesDeleteSuccessAction,
    (state, action) => state.filter((id) => action.payload.result.indexOf(id) === -1),
  ),
);

