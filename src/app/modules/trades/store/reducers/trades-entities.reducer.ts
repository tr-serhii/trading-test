import { TradesDictionary } from '@modules/trades/types';
import { createReducer, on } from '@ngrx/store';
import {
  tradesCreateSuccessAction,
  tradesItemSuccessAction,
  tradesListSuccessAction,
  tradesUpdateSuccessAction,
} from '../actions';

export const tradesEntitiesReducer = createReducer<TradesDictionary>(
  {},
  on(
    tradesListSuccessAction,
    (state, action) => {
      const entities = action.payload.entities.trades;
      return { ...state, ...entities };
    },
  ),
  on(
    tradesItemSuccessAction,
    tradesUpdateSuccessAction,
    tradesCreateSuccessAction,
    (state, action) => ({ ...state, ...action.payload.entities.trades }),
  ),
);
