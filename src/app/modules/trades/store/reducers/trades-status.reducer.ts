import {
  tradesCreateErrorAction,
  tradesCreateRequestAction,
  tradesCreateSuccessAction,
  tradesDeleteErrorAction,
  tradesDeleteRequestAction,
  tradesDeleteSuccessAction,
  tradesItemErrorAction,
  tradesItemRequestAction,
  tradesItemSuccessAction,
  tradesListErrorAction,
  tradesListRequestAction,
  tradesListSuccessAction,
  tradesUpdateErrorAction,
  tradesUpdateRequestAction,
  tradesUpdateSuccessAction,
} from '@modules/trades/store/actions';
import {
  RequestStateStatus,
  RequestStateStatusLabel,
  RequestStatusState,
} from '@modules/trades/types';
import { createReducer, on } from '@ngrx/store';

export const tradesStatusReducer = createReducer<RequestStatusState>(
  {} as RequestStatusState,
  on(tradesListSuccessAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.list]: RequestStateStatus.success,
  })),
  on(tradesListRequestAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.list]: RequestStateStatus.pending,
  })),
  on(tradesListErrorAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.list]: RequestStateStatus.error,
  })),

  on(tradesItemSuccessAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.item]: RequestStateStatus.success,
  })),
  on(tradesItemRequestAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.item]: RequestStateStatus.pending,
  })),
  on(tradesItemErrorAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.item]: RequestStateStatus.error,
  })),

  on(tradesUpdateSuccessAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.update]: RequestStateStatus.success,
  })),
  on(tradesUpdateRequestAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.update]: RequestStateStatus.pending,
  })),
  on(tradesUpdateErrorAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.update]: RequestStateStatus.error,
  })),

  on(tradesCreateSuccessAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.create]: RequestStateStatus.success,
  })),
  on(tradesCreateRequestAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.create]: RequestStateStatus.pending,
  })),
  on(tradesCreateErrorAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.create]: RequestStateStatus.error,
  })),

  on(tradesDeleteSuccessAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.delete]: RequestStateStatus.success,
  })),
  on(tradesDeleteRequestAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.delete]: RequestStateStatus.pending,
  })),
  on(tradesDeleteErrorAction, (state) => ({
    ...state,
    [RequestStateStatusLabel.delete]: RequestStateStatus.error,
  })),
);
