import {
  tradesCreateErrorAction,
  tradesCreateRequestAction,
  tradesCreateSuccessAction,
  tradesItemErrorAction,
  tradesItemRequestAction,
  tradesItemSuccessAction,
  tradesListErrorAction,
  tradesListRequestAction,
  tradesListSuccessAction,
  tradesUpdateErrorAction,
  tradesUpdateRequestAction,
  tradesUpdateSuccessAction,
} from '@modules/trades/store/actions';
import { createReducer, on } from '@ngrx/store';
import { RequestError } from '@modules/trades/types';

export const tradesErrorReducer = createReducer<RequestError | null>(
  null,
  on(
    tradesListSuccessAction,
    tradesItemSuccessAction,
    tradesCreateSuccessAction,
    tradesUpdateSuccessAction,
    () => null,
  ),
  on(
    tradesListRequestAction,
    tradesItemRequestAction,
    tradesCreateRequestAction,
    tradesUpdateRequestAction,
    () => null,
  ),
  on(
    tradesListErrorAction,
    tradesItemErrorAction,
    tradesCreateErrorAction,
    tradesUpdateErrorAction,
    (state, action) => action.payload,
  ),
);
