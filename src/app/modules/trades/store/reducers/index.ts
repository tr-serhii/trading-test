import { combineReducers } from '@ngrx/store';
import { TradesState } from '../../types';
import { tradesEntitiesReducer } from './trades-entities.reducer';
import { tradesErrorReducer } from './trades-error.reducer';
import { tradesIdsReducer } from './trades-ids.reducer';
import { tradesSelectedIdReducer } from './trades-selectedId.reducer';
import { tradesStatusReducer } from './trades-status.reducer';

export const tradesReducer = combineReducers<TradesState>({
  ids: tradesIdsReducer,
  entities: tradesEntitiesReducer,
  status: tradesStatusReducer,
  error: tradesErrorReducer,
  selectId: tradesSelectedIdReducer,
});
