import { createReducer, on } from '@ngrx/store';
import {
  tradesCreateSuccessAction,
  tradesItemRequestAction,
  tradesItemResetAction,
} from '@modules/trades/store/actions';

export const tradesSelectedIdReducer = createReducer<string | null>(
  null,
  on(
    tradesItemRequestAction,
    (state, action) => action.payload,
  ),
  on(
    tradesCreateSuccessAction,
    (state, action) => action.payload.result as string,
  ),
  on(
    tradesItemResetAction,
    (state, action) => null,
  ),
);
