import { TradesEntityData, RequestError } from '@modules/trades/types';
import { createAction, props } from '@ngrx/store';

// Get all trades action
export const tradesListRequestAction = createAction(
  '[trades] list request',
  props<{ payload?: { reset?: boolean } }>(),
);
export const tradesListSuccessAction = createAction('[trades] list success', props<{ payload: TradesEntityData }>());
export const tradesListErrorAction = createAction('[trades] list error', props<{ payload: RequestError }>());
