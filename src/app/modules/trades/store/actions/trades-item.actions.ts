import { TradeFormRequest, TradesEntityData, RequestError } from '@modules/trades/types';
import { createAction, props } from '@ngrx/store';
// get one
export const tradesItemRequestAction = createAction('[trades] item request', props<{ payload: string }>());
export const tradesItemSuccessAction = createAction(
  '[trades] item success',
  props<{ payload: TradesEntityData }>(),
);
export const tradesItemErrorAction = createAction('[trades] item error', props<{ payload: RequestError }>());

export const tradesItemResetAction = createAction('[trades] item reset');

// update
export const tradesUpdateRequestAction = createAction(
  '[trades] update request',
  props<{ payload: { id: string; data: TradeFormRequest } }>(),
);
export const tradesUpdateSuccessAction = createAction(
  '[trades] update success',
  props<{ payload: TradesEntityData }>(),
);
export const tradesUpdateErrorAction = createAction('[trades] update error', props<{ payload: RequestError }>());

// create
export const tradesCreateRequestAction = createAction(
  '[trades] create request', props<{ payload: TradeFormRequest }>());
export const tradesCreateSuccessAction = createAction(
  '[trades] create success', props<{ payload: TradesEntityData }>());
export const tradesCreateErrorAction = createAction('[trades] create error', props<{ payload: RequestError }>());

// delete
export const tradesDeleteRequestAction = createAction('[trades] delete request', props<{ payload: string }>());
export const tradesDeleteSuccessAction = createAction(
  '[trades] delete success',
  props<{ payload: TradesEntityData }>(),
);
export const tradesDeleteErrorAction = createAction('[trades] delete error', props<{ payload: RequestError }>());

