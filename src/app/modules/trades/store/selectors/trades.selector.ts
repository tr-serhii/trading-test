import { tradesSchema } from '@modules/trades/store/schemas';
import {
  TradesState,
  RequestStateStatus,
  RequestStateStatusLabel,
} from '@modules/trades/types';
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { denormalize } from 'normalizr';

export const tradesSelector = createFeatureSelector<TradesState>('trades');

export const selectTradesIds = createSelector(
  tradesSelector,
  (store: TradesState) => store.ids,
);

export const selectTradesEntities = createSelector(
  tradesSelector,
  (store: TradesState) => store.entities,
);

export const selectTradesError = createSelector(
  tradesSelector,
  (store: TradesState) => store.error,
);

export const tradesStatusSelector = (label: RequestStateStatusLabel): MemoizedSelector<TradesState, RequestStateStatus> => createSelector(
  tradesSelector,
  (store: TradesState): RequestStateStatus => store.status?.[label],
);

export const tradesStatusSomeSelector = (status: RequestStateStatus): MemoizedSelector<TradesState, boolean> => createSelector(
  tradesSelector,
  (store: TradesState): boolean => {
    if (!store.status) {
      return false;
    }

    return Object.values(store.status).some((label) => label === status);
  },
);

export const selectTradesSelectedId = createSelector(
  tradesSelector,
  (store: TradesState) => store.selectId,
);

export const selectTradesList = createSelector(
  [selectTradesIds, selectTradesEntities],
  (ids, trades) => denormalize(ids, [tradesSchema], { trades }),
);

export const selectTradesItem = createSelector(
  [selectTradesSelectedId, selectTradesEntities],
  (id, trades) => denormalize(id, tradesSchema, { trades }),
);


