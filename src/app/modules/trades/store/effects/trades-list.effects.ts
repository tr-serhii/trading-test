import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TradesApiService } from '@modules/trades/services';
import {
  tradesListErrorAction,
  tradesListRequestAction,
  tradesListSuccessAction,
} from '@modules/trades/store/actions';
import { tradesSchema } from '@modules/trades/store/schemas';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { normalize } from 'normalizr';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';

@Injectable()
export class TradesListEffects {

  public list$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        tradesListRequestAction,
      ),
      exhaustMap(() => {
          return this.tradesApiService.list().pipe(
            map((response) => ({
              type: tradesListSuccessAction.type,
              payload: {
                ...normalize(response.data, [tradesSchema]),
              },
            })),
            catchError((err: HttpErrorResponse) => of({
              type: tradesListErrorAction.type,
              payload: err.error,
            })),
          );
        },
      ),
    ),
  );

  public constructor(
    private actions$: Actions,
    private tradesApiService: TradesApiService,
  ) { }
}
