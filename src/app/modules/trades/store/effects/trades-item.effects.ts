import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TradesApiService } from '@modules/trades/services';
import {
  tradesCreateErrorAction,
  tradesCreateRequestAction,
  tradesCreateSuccessAction,
  tradesDeleteErrorAction,
  tradesDeleteRequestAction,
  tradesDeleteSuccessAction,
  tradesItemErrorAction,
  tradesItemRequestAction,
  tradesItemSuccessAction,
  tradesUpdateErrorAction,
  tradesUpdateRequestAction,
  tradesUpdateSuccessAction,
} from '@modules/trades/store/actions';
import { tradesSchema } from '@modules/trades/store/schemas';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { normalize } from 'normalizr';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';

@Injectable()
export class TradesItemEffects {
  public item$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tradesItemRequestAction),
      exhaustMap(({payload}) =>
        this.tradesApiService.item(payload).pipe(
          map((response) => ({
            type: tradesItemSuccessAction.type,
            payload: normalize(response, tradesSchema),
          })),

          catchError((err: HttpErrorResponse) => of({
            type: tradesItemErrorAction.type,
            payload: err.error,
          })),
        ),
      ),
    ),
  );

  public update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tradesUpdateRequestAction),
      exhaustMap(({payload: {id, data}}) =>
        this.tradesApiService.update(id, data).pipe(
          map((response) => ({
            type: tradesUpdateSuccessAction.type,
            payload: normalize(response, tradesSchema),
          })),

          catchError((err: HttpErrorResponse) => of({
            type: tradesUpdateErrorAction.type,
            payload: err.error,
          })),
        ),
      ),
    ),
  );

  public create$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tradesCreateRequestAction),
      exhaustMap(({payload}) =>
        this.tradesApiService.create(payload).pipe(
          map((response) => ({
            type: tradesCreateSuccessAction.type,
            payload: normalize(response, tradesSchema),
          })),

          catchError((err: HttpErrorResponse) => of({
            type: tradesCreateErrorAction.type,
            payload: err.error,
          })),
        ),
      ),
    ),
  );

  public delete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(tradesDeleteRequestAction),
      exhaustMap(({payload}) =>
        this.tradesApiService.delete(payload).pipe(
          map((response) => ({
            type: tradesDeleteSuccessAction.type,
            payload: normalize(response, tradesSchema),
          })),

          catchError((err: HttpErrorResponse) => of({
            type: tradesDeleteErrorAction.type,
            payload: err.error,
          })),
        ),
      ),
    ),
  );

  public constructor(
    private actions$: Actions,
    private tradesApiService: TradesApiService,
  ) {
  }
}
