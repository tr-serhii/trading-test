import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { TradesService } from '@modules/trades/services';

@Injectable({
  providedIn: 'root',
})
export class TradesItemResolver implements Resolve<any> {
  public constructor(
    private tradesService: TradesService,
  ) { }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): void {
    const id = route.paramMap.get('id');

    if (id) {
      return this.tradesService.item(id);
    }

    this.tradesService.itemReset();
  }
}
