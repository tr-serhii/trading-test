import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { TradesService } from '@modules/trades/services';

@Injectable({
  providedIn: 'root',
})
export class TradesListResolver implements Resolve<any> {
  public constructor(
    private tradesService: TradesService,
  ) { }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): void {
    return this.tradesService.list(true);
  }
}
